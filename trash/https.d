import std.net.curl;
import std.stdio;
import std.string;
import std.conv;
import core.thread;
import httpresponse;


class HTTPS {
	private string base;
	private response resp;
	private ubyte[] data;
	
	
	
	public this(string host, ushort port) {
		this.base = "https://" ~ host ~ ":" ~ to!string(port);
		writeln("base "~this.base);
	}
	
	public bool connect() {
		// the ssl mode doesn't keep the connection :/ TODO: lowlevel optimized SSL, by now this soft is optimized to http connections not https. 
		return true;
	}
	
	public bool reconnect() {
		return true;
	}
	
	public void setHeader(string header) {
		// fake setHeader by now
	}
	
	public void head(string path) {
		this.get(path); // TODO: specify method
	}
	

	
	public void get(string path) {
		data = [];
		resp.path = this.base ~ path;
		writeln(resp.path);
		resp.sz = 0;
		writeln(resp.path);
		auto https = HTTP(resp.path);
		https.onReceiveHeader = (in char[] key, in char[] value) { resp.headers[cast(string)key]=cast(string)value; };
		https.onReceive = (ubyte[] data) { this.data ~= data;  return data.length; };
		https.perform();
	}
	
	public response read() {  // what if the asinc onRecive hasn't finished?
		
		while (data.length == 0) {
			Thread.sleep( dur!("seconds")( 1 ) );
		};
		
		char* cstr = cast(char*)data;
		resp.html = cast(string) cstr[0..data.length];
		resp.sz = data.length;
		
		return resp;
	}
	
	public void close() {
	}
	
}
