import http;
import https;
import url;
import httpresponse;

class Connect {
	private HTTP http;
	private HTTPS https;
	private bool ssl;
	private bool useHead;

	public this(URL urlbase, bool useHead) {
		this.useHead = useHead;
		ssl = urlbase.isSSL();
		
		if (ssl) {
			https = new HTTPS(urlbase.getHost(), urlbase.getPort());
			
		} else {
			http = new HTTP(urlbase.getHost(), urlbase.getPort()); 
			http.setHeader("Connection: Keep-Alive");
			if (!http.connect()) {
				assert(1==2); //TODO: throw new Exception("lalala"):
			}
		}
	
	}
	
	public response send(string path) {
		if (ssl) {
			if (useHead)
				https.head(path);
			else
				https.get(path);
				
			return https.read();
				
		} else {

			if (useHead)
				http.head(path);
			else
				http.get(path);
				
			return http.read();
		}
	}

	public response get(string url) {
		return this.send(url);
	}

	public response post(string url, string post) {
		if (ssl) {
			https.post(url,post);
			return https.read();
		} else {
			http.post(url,post);
			return http.read();
		}
	}


	
	public void close() {
		if (ssl)
			https.close();
		else
			http.close();
	}
	
	
}
