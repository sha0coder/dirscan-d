/*
	@sha0coder
*/

import std.conv;
import std.stdio;
import std.array;
import std.stream;
import core.thread;
import std.string;
import std.socket;
import std.socketstream;
import url;
import httpresponse;

import core.memory;

enum connStat {
	DISCONNECTED=1,
	CANSEND=2, 
	MUSTREAD=3,  
};

class HTTP {
	private Address target;
	private string host;
	private Socket sock;
	private Stream stream;
	private string[] headers; 
	private ushort port;
	private string path;
	private string method;
	private int stat;

	public this(string host, ushort port) {
		this.host = host;
		this.port = port;
		this.stat = connStat.DISCONNECTED;
		//GC.disable();

		init(host,port);
	}	

	private void init(ref string host, ref ushort port) {
		sock = new TcpSocket();
		sock.blocking(true);
		stream = new SocketStream(sock);
	}

	public bool connect() { // only called once, use reconnect()
		try {

			Address[] ips = getAddress(host, port);
			target = ips[0];
			sock.connect(target);

		} catch (SocketException e) {
			writeln("Can't connect");
			return false;
		}

		stat = connStat.CANSEND;
		return true;
	}

	public bool reconnect() {
		stat = connStat.DISCONNECTED;

		try {
			sock.shutdown(SocketShutdown.BOTH);
			stream.close();
			sock.close();

			sock = new TcpSocket();
			sock.blocking(true);
			stream = new SocketStream(sock);

			sock.connect(target);

		} catch (SocketException e) {
			writeln("Can't connect");
			return false;
		}

		stat = connStat.CANSEND;
		return true;
	}

	public void setHeader(string header) {
		int i = this.headers.length++;
		this.headers[i] = header;
	}

	private void send(string method, string path) {
		if (!sock.isAlive())
			reconnect();


		this.path = path;
		string request = method ~ " " ~ path ~ " HTTP/1.1\r\nHost: "~ this.host ~ "\r\n";
		foreach (hdr; this.headers)
			request ~= hdr ~ "\r\n";
		request ~= "\r\n";
		stream.writeString(request);
		this.method = method;

		stat = connStat.MUSTREAD;
	}

	public void head(string path) {
		this.send("HEAD",path);
	}

	public void get(string path) {
		this.send("GET",path);
	}

	/*
	public void post(string path, stirng data) {
		//TODO
	}*/


	public response read() { 		// TODO: try/catch
		string key,value;
		response resp;
		int stat = 0;
		char[][]spl;
		char[] line;

		resp.sz = 0;
		resp.code = 0;
		resp.html = "";
		resp.path = path;
		//resp.url = new URL("http://"~host~":"~to!string(port)~path);


		//while (stream.eof()) {}


		while (!stream.eof()) {
			line = stream.readLine();
			//writeln("->"~line~"<-");

			if (line.length == 0)
				stat = 2;

			else {

				if (stat == 0) {
					spl = split(line, " ");
					resp.code = to!uint(spl[1]);
					//writeln(line);
					stat++;

				} else if (stat == 1) {
					spl = split(line, ": ");
					key = cast(string)spl[0];
					value = cast(string)spl[1];

					switch(toLower(key)) {
						default:
							break;

						case "location":
							resp.headers["location"] = value;
							break;

						case "server":
							resp.headers["server"] = value;
							break;

						case "content-length":
							resp.headers["content-length"] = value;
							break;

						case "content-type":
							resp.headers["content-type"] = value;
							break;

						case "cache-control":
							resp.headers["cache-control"] = value;
							break;

						case "date":
							resp.headers["date"] =  cast(string)replace(line, "Date: ", "");
							break;

						case "expires":
							resp.headers["expires"] = cast(string)replace(line, "Expires: ", "");
							break;

						case "connection":
							resp.headers["connection"] = value;
							break;
					}

				} else if (stat == 2) {
					resp.sz += line.length;
					resp.html ~= line;
				}
				
			}

			if (method == "HEAD" && line == "")
				break;

			if (endsWith(line,"</html>")) {
				//writeln("closing html");
				break;
			}

		}

		if ("connection" in resp.headers) 
			if (resp.headers["connection"] == "close") 
				reconnect();

		stat = connStat.CANSEND;
		return resp;
	}


	/*
	public response read() { 		// TODO: try/catch
		string key,value;
		response resp;
		int stat = 0;
		char[][]spl;

		resp.sz = 0;
		resp.code = 0;
		resp.html = "";
		resp.url = new URL("http://"~host~":"~to!string(port)~path);

		foreach(char[] line; this.stream) {
			writeln(line);
			writeln(stream.eof());

			if (line.length == 0)
				stat = 2;

			else {

				if (stat == 0) {
					spl = split(line, " ");
					resp.code = to!uint(spl[1]);
					//writeln(line);
					stat++;

				} else if (stat == 1) {
					spl = split(line, ": ");
					key = cast(string)spl[0];
					value = cast(string)spl[1];

					switch(toLower(key)) {
						default:
							break;

						case "location":
							resp.headers["location"] = value;
							break;

						case "server":
							resp.headers["server"] = value;
							break;

						case "content-length":
							resp.headers["content-length"] = value;
							break;

						case "content-type":
							resp.headers["content-type"] = value;
							break;

						case "cache-control":
							resp.headers["cache-control"] = value;
							break;

						case "date":
							resp.headers["date"] =  cast(string)replace(line, "Date: ", "");
							break;

						case "expires":
							resp.headers["expires"] = cast(string)replace(line, "Expires: ", "");
							break;
					}

				} else if (stat == 2) {
					resp.sz += line.length;
					resp.html ~= line;
				}
				
			}

		}
		
		writeln("read end");
		stat = connStat.CANSEND;
		return resp;
	}*/




	public void close() {
		try {
			this.stream.close();
			this.sock.close();
		} finally {
			stat = connStat.DISCONNECTED;
		}
	}


}


// TEST






	/*

	foreach (int i, Address a; ips) {
		writefln("  Address %d:", i+1);
		writefln("    IP address: %s", a.toAddrString());
		writefln("    Hostname: %s", a.toHostNameString());
		writefln("    Port: %s", a.toPortString());
		writefln("    Service name: %s", a.toServiceNameString());
	}*/
