/*
    from Teoh http://forum.dlang.org/thread/yrxspdrpusrrijmfyldc@forum.dlang.org?page=1

    Lifo power, fast stack and priorize last inputs for parsing the directory tree in depht way.
*/


struct Lifo(E) {
    private E[] impl;
    private size_t curSize = 0;

    @property bool empty() { 
    	return (curSize == 0); 
    }

    @property ref E top() {
		assert(curSize > 0);
		return impl[curSize-1];
    }

    size_t size() {
    	return curSize;
    }

    E pop()	{
		assert(curSize > 0);
		auto elem = top();
		curSize--;
		return elem;
    }

    void push(E elem) {
		if (curSize == impl.length) {
		    // Array not big enough to fit element; increase capacity.
		    impl.length = (curSize + 1)*2;
		}
		assert(curSize < impl.length);
		impl[curSize] = elem;
		curSize++;
    }

}
