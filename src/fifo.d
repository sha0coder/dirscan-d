
class Fifo(T)
{
        this()
        {
                array.length = 4;
                num = 0;
        }

        void add(T t)
        {
                if (num >= array.length) {
                        array.length = array.length * 2 + 1;
                }

                array[num++] = t;
        }

        T pop()
        {
                if (num == 0)
                        return null;

                T t = array[0];
                array[0] = null;
                array = array[1 .. array.length];

                num--;

                return t;
        }

        size_t length()
        {
                return num;
        }

protected:
        size_t num;
        T array[];
}

/*
struct Fifo(E) {
    private E[] impl;

    @property bool empty() { 
    	return (impl.length == 0); 
    }

    size_t size() {
    	return impl.length;
    }

    E get()	{
		assert(size() > 0);
		auto elem = top();
		curSize--;
		return elem;
    }

    void put(E elem) {
		if (curSize == impl.length) {
		    // Array not big enough to fit element; increase capacity.
		    impl.length = (curSize + 1)*2;
		}
		assert(curSize < impl.length);
		impl[curSize] = elem;
		curSize++;
    }

}*/
