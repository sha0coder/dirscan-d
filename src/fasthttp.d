import std.stdio;

import socketpool;


class FastHTTP : SocketPool {
	private string ua = "";

	this(int pool) {
		super(pool);
	}
	
	public void get(string path) {
		queue.push("HEAD /"~path~" HTTP/1.1\nHost: "~host~"\nConnection: Keep-Alive\n\n");
	}
	
	public void head(string path) {
		queue.push("HEAD /"~path~" HTTP/1.1\nHost: "~host~"\nConnection: Keep-Alive\n\n");
	}

	override protected void parse(string request, string response) {
		writefln("req: %s\nresp: %s", request, response);
	}	
	
}
