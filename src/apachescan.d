// apache vuln checker by jo

import std.net.curl;
import std.stdio;
import std.conv;

/* Affected and tested versions
PHP 5.3.10
PHP 5.3.8-1
PHP 5.3.6-13
PHP 5.3.3
PHP 5.2.17
PHP 5.2.11
PHP 5.2.6-3
PHP 5.2.6+lenny16 with Suhosin-Patch
Affected versions
PHP prior to 5.3.12
PHP prior to 5.4.2
Unaffected versions
PHP 4 - getopt parser unexploitable
PHP 5.3.12 and up
PHP 5.4.2 and up
Unaffected versions are patched by CVE-2012-1823.
*/



const string[] targets = [
	"/cgi-bin/",
	"/cgi-bin/php",
	"/cgi-bin/php5",
	"/cgi-bin/php-cgi",
	"/cgi-bin/php.cgi",
	"/cgi-bin/php4",
	"/cgi/",
	"/cgi/php",
	"/cgi/php5",
	"/cgi/php-cgi",
	"/cgi/php.cgi",
	"/cgi/php4",
];


void main(string[] args) {
	char[] html;
	HTTP conn = HTTP();
	
	foreach(t; targets) {
		try {
			html = get(args[1]~t, conn);
			writeln(args[1]~t ~ " 200 ok!!!!!!!!!!");
			writeln(html);
		} catch(CurlException e) {
			if (conn.statusLine().code == 403) 
				writeln(args[1]~t~" 403 !!!!!!!");								
			else
				writeln(args[1]~t~" "~to!string(conn.statusLine().code));
				
		}  catch(Exception e) {
			writeln(args[1]~t~ "exception: ");
			writeln(e);
		}
	}
}