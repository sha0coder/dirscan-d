/*
	Jobs()  multiple threading control by @sha0coder
*/

import core.thread;



class Jobs(T) {
	
	private T[] threads;
	
	public void add(T t) {
		this.threads ~= t;
	}
	
	public void start() {
		foreach (t;this.threads) {
			t.start();
		}
	}
	
	public void stop() {
		foreach (t;this.threads) {
			t.stop();
		}
	}
	
	public void join() {
		foreach (t;this.threads) {
			//if (t.isRunning()) 
			t.join();
		}
	}
	
}