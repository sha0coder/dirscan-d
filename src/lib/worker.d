import std.conv;
import std.file;
import std.stdio;
import core.thread;
import std.net.curl;

import url;
import html;
import lifo;
import color;
import settings;


enum wstats {
	STOPPED = 0,
	PROCESSING = 1,
	PENDING = 2,
	STOPPING = 3
};

class Worker : Thread {
	private URL initUrl;
	private ushort port;
	private string host;
	private int wstat;
	private bool wRunning;
	private size_t wid;
	private string[] words;
	private Lifo!string queue;
	private Settings settings;
	private size_t page404;
	private void delegate(string) allpush;
	private HTTP conn;


	public this(URL initUrl, string[] wordlist, Settings settings, void delegate(string) push, size_t page404, size_t wid) {
		this.initUrl = initUrl;
		this.wid = wid;
		this.words = wordlist;
		this.settings = settings;
		this.allpush = push;
		this.page404 = page404;
		this.wRunning = false;
		wstat = wstats.STOPPED;
		
		this.initNetwork();
		super(&run);
	}
	
	
	private void initNetwork() {
		this.conn = HTTP();
		this.conn.addRequestHeader("User-agent","Mozilla/5.0 (Windows NT 6.1; rv:20.0) Gecko/20100101 Firefox/20.0");
		this.conn.addRequestHeader("Content-type","application/x-www-form-urlencoded");
		this.conn.addRequestHeader("Connection","keepalive");
		this.conn.connectTimeout(dur!"seconds"(5));
		this.conn.dnsTimeout(dur!"seconds"(10));
		this.conn.dataTimeout(dur!"seconds"(10));
		//conn = new Connect(initUrl, (page404==-1));
	}
	
	public size_t getId() {
		return wid;
	}

	public size_t pending() {
		return queue.size();
	}

	private void run() {
		wRunning = true;
 
		while (wRunning) {
	
			//printf("(%d) processing queue:%d\n",wid,queue.size());
			wstat = wstats.PROCESSING;
			while (queue.size()>0)  {
				string url = this.initUrl.getUrl()~queue.pop();
				writeln(url);
				this.conn = HTTP();
				auto html = get(url, this.conn);
				classify(url,html);
				
				//printf("(%d) pending\n",wid);
				wstat = wstats.PENDING;
				Thread.sleep(dur!("msecs")(2000));
			}
		}
		
		wstat = wstats.STOPPED;
	}

	private void show(string col, string msg) {
		//printf("%s%s%d: ",paint.clear.ptr,col.ptr,wid);

		printf("%s%s",paint.clear.ptr,col.ptr);
		writef(msg);
		printf("%s\n",paint.clean.ptr);

		//if (settings.getLogfile() != "")
		//	std.stdio.write(settings.getLogfile(), msg);
	}

	// called by dirscan object
	public void push(string path) {
		foreach (word; words) {
			foreach (ext; settings.getExtensions()) {
				queue.push(path ~ word ~ ext);
			}
		}
	}

	private void classify(string path, char[] html) { // TODO: el code en string me ahorro conversiones
		URL url;
		//writeln(resp.path~" "~to!string(resp.code));


		if (this.conn.statusLine().code == 0) {
			show(paint.red,"[timeout] "~path);
			//writeln(resp);
			return;
		}

		if (page404 == html.length)
			this.conn.statusLine().code = 404;

		if (this.conn.statusLine().code != 404) 
			url = new URL(path);

		switch(this.conn.statusLine().code) {
			case 200:
				HTML page = new HTML(to!string(html), url);
				if (page.isDirlisting()) {
					show(paint.green,"[200 Ok Dirlisting]\t"~url.getUrl());
					// crawl
				} else {
					show(paint.green,"[200 Ok] (" ~ to!string(html.length) ~ " bytes)\t" ~ url.getUrl());
					if (url.isDirectory()) {
						allpush(url.getPath());
					} // else crawl
				}
				
				break;

			case 301:
			case 302:
			case 303:
				show(paint.cyan, "[" ~ to!string(this.conn.statusLine().code) ~ "Redirect]\t\t" ~ url.getUrl() ~ " -> " ~ this.conn.responseHeaders()["location"]);
				auto loc = new URL(this.conn.responseHeaders()["location"]);
				if (loc.getHost() == url.getHost && 
					this.conn.responseHeaders()["location"] != url.getUrl() && 
					loc.getPort() == url.getPort()) 
						this.classify(loc.getPath(),get(loc.getPath(),this.conn)); // limitar recursividad? 
				break;

			case 401:
				show(paint.magenta, "[401 Auth needed]\t"~url.getUrl());
				break;

			case 403:
				auto resp2 = get(path~"/ajoeifjaweofiaj",this.conn);

				if (this.conn.statusLine().code == 404) {
					show(paint.yellow, "[403 Dirlisting denied]\t"~url.getUrl());
					allpush(url.getPath());
				} else 
					show(paint.yellow, "[403 Permission denied]\t"~url.getUrl());
				break;

			case 404:
				break;

			default:
				show(paint.blue,"[" ~ to!string(this.conn.statusLine().code) ~ "]  \t" ~ url.getUrl());
				break;
		}
	}


	public void stop() {
		wstat = wstats.STOPPING;
		wRunning = false;
	}
}

