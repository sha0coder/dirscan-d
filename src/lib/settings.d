import std.string;
import std.stdio;
import std.array;
import std.file;
import std.conv;

class Settings {
	private string[string] sett;

	private void load(string filename) {
		try {
			auto file = File(filename);

			foreach(line; file.byLine) {
				line = replace(line, " ", "");
				line = replace(line, "\t", "");
				if (line.length == 0 || line[0] == '#')
					continue;

				char[][] spl = split(line, ":");

				if (spl.length == 2) {
					immutable string key = spl[0].idup;
					sett[key] = cast(string)spl[1];
				}

			}

			file.close();

		} catch (Exception e) {
			writeln("Error loading settings file.");
			writeln(e.msg);
		}
	}

	private void defaults() {
		sett["log"] = "";
		sett["wordlist"] = "wordlists/dirs.txt";
		sett["extensions"] = ".php";
		sett["threads"] = "5";
		sett["verbose"] = "no";
	}

	public this(string filename) {
		if (exists(filename) <=0 ) {
			writefln("%s config file doesn't exists.",filename);
			return;
		}

		defaults();
		load(filename);
	}

	public string getWordlist() {
		return sett["wordlist"];
	}

	public string[] getExtensions() {
		string[] extensions = ["/"];
		char[][] spl = split(cast(char[])sett["extensions"], ",");
		foreach (ext; spl) {
			extensions ~= ext.idup;
		}
		return extensions;
	}

	public uint getThreads() {
		return to!uint(sett["threads"]);
	}

	public string getLogfile() {
		return sett["log"];
	}

	public bool isVerbose() {
		return (sett["verbose"]=="yes");
	}
}


