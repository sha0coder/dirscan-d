import std.stdio;
import std.array;

class Wordlist {

	private string[] wl;

	private void put(string item) {
		this.wl ~= item;
	}

	public void load(string filename) {
		string line;

		try {
			File *f = new File(filename,"r");
			while ((line = f.readln()) != "") {
				//char []l = cast(char[])line;
				line = cast(string)replace(cast(char[])line.idup,"\n","");
				put(line);
			}
			f.close();

			writefln("%d words loaded.",wl.length);

		} catch (StdioException e) {
			writeln("File not found");
		} catch (Exception e) {
			writeln(e.msg);
		}
	}

	public void print() {
		foreach(w; this.wl) {
			writeln(w);
		}
	}

	public size_t getSize() {
		return wl.length;
	}

	public string[] getChunk(size_t start, size_t end) {
		return wl[start..end];
	}

	public string[] get() {
		return wl;
	}
}

