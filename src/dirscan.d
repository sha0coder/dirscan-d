/*
	Web Directory Scanner


	@sha0coder
	jesus.olmos at blueliv.com

	TODO:
		- handle ^C -> (clean y clear y stop workers)
		- cambiar asserts por throw new Exception("http:// expected");

*/

import std.net.curl;
import std.string;
import std.stdio;
import std.array;
import std.file;
import std.conv;


import settings;
import wordlist;
import worker;
import url;

import core.thread;


class DirScan {
	private Wordlist wordlist;
	private Worker[] workers;
	private Settings settings;
	private URL initUrl;

	private void addWorker(Worker worker) {
		//int id = this.workers.length++;
		this.workers ~= worker;
	}

	public this(string initUrl) {
		this.initUrl = new URL(initUrl);

		settings = new Settings("dirscan.conf");
		wordlist = new Wordlist();
		wordlist.load(settings.getWordlist());
	} 

	public void push(string urldir) {
		foreach(w; workers)
			w.push(urldir);
	}

	private size_t test404() {
		writeln("Analyzing site ...");
		
		HTTP conn = HTTP();
		conn.addRequestHeader("User-agent","Mozilla/5.0 (Windows NT 6.1; rv:20.0) Gecko/20100101 Firefox/20.0");
		conn.addRequestHeader("Content-type","application/x-www-form-urlencoded");
		
		//string url = this.initUrl.getUrl() ~ "/jaoweifoeijawef";
		auto resp = get("http://google.com", conn);
		
		if (conn.statusLine().code != 404) {
			writeln("Customized 404 page detected.");
			return resp.length;
			
		} else {
			writeln("No 404 custom page.");
			return -1;
		}
	}

	public void start() {
		bool running = true;
		uint th = settings.getThreads();
		size_t sz = wordlist.getSize();
		size_t r = (sz / th);
		size_t start = 0;
		size_t end;
		size_t page404 = test404();

		if (settings.isVerbose())
			writeln("Preparing workers ...");
		
		foreach (i; 0 .. settings.getThreads()) {
			end = start+r;
			addWorker(new Worker(initUrl, wordlist.getChunk(start,end), settings, &push, page404, workers.length));
			start = end;
		}

		if (end+1 < sz) {
			addWorker(new Worker(initUrl, wordlist.get()[start .. $], settings, &push, page404, workers.length));
		}

		foreach(w; workers) {
			w.push("/");
			w.start();
		}
		
		writeln(to!string(workers.length) ~ " workers running ...");

		while(running) {
			Thread.sleep( dur!("seconds")(20)); // TODO: better timeouted join?
			
			// Check if any worker is working 
			running = false;
			foreach (w; workers) {
				if (w.pending() > 0)
					running = true;
			}
		}

		// stopping ...
		writeln("stopping workers & closing connections ...");
		foreach (w; workers) {
			w.stop();
		}
		writeln("end.");


	}


}



void main(string[] args) {
	
	if (args.length == 2) {
	
		DirScan dirscan = new DirScan(args[1]);
		dirscan.start();

	} else {
		writefln("USAGE %s [url]",args[0]);
	} 

}
