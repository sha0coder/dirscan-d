/*
	Fast single threaded asynchronous socket pool.
	@sha0coder
*/

import std.socket;
import std.stdio;
import lifo;

class SocketPool {
	private int buff_sz = 1024;
	private Socket[] sockets;
	private string[] pending; 
	private int sz;
	private int timeout = 5;
	protected Lifo!string queue;
	protected string host;
	protected ushort port;

	
	this(int pool) {
		sz = pool;
		sockets = [];
		pending = [];
	
		foreach(i;0..pool) {
			Socket sock = new Socket(AddressFamily.INET,SocketType.STREAM);
			sock.blocking(false);
			sock.setKeepAlive(10,10);
			sock.setOption(SocketOptionLevel.SOCKET, SocketOption.REUSEADDR, true);
			sockets ~= sock;
			pending ~= "";
		}
	}
	
	public void setTimeout(int t) {
		timeout = t;
	}
	
	public void setBuffSize(int sz) {
		buff_sz = sz;
	}
	
	public void connect(string ip, ushort port) {	
		host = ip;
		foreach(ref s; sockets) 
			s.connect(new InternetAddress(ip,port));
		writeln("All pool connected");
	}
	
	public void close() {
		foreach(ref s; sockets)
			s.close();
	}
	
	
	abstract protected void parse(string request, string response);
	
	
	public void loop() {
		SocketSet read = new SocketSet();
		SocketSet write = new SocketSet();
		SocketSet error = new SocketSet();
		auto buff = new byte[buff_sz];
			
		int c;
		while (1) {
			read.reset();
			write.reset();
			error.reset();
			foreach(ref s; sockets) {
				s.blocking(false);
				read.add(s);
				write.add(s);
				error.add(s);
			}
			
			c = Socket.select(read, write, error, dur!"seconds"(timeout));
			if (c == -1) {	
				writeln("select error");
				break;
				
			} else if (c == 0) {
				writeln("timeout, chequear si los sockets están ok?");
							
				/* RECONNECT
				
				close();
				init();
 				connect();
				foreach(ref s;sockets) {
					read.add(s);
					write.add(s);
					error.add(s);
				}*/
				
			} else if (c>0) {
				
				foreach (i; 0..sockets.length) {
					
					// if alive()
					
					
					if (read.isSet(sockets[i])) {
											
						auto b = sockets[i].receive(buff);
						if (b>0) {
							parse(pending[i],cast(string)buff);
							pending[i] = "";
						}
						
					} else if (write.isSet(sockets[i])) {
						if (queue.size() > 0 && pending[i].length == 0) {
							string item = queue.pop();
							//writeln(" send "~item);
							pending[i] = item;
							sockets[i].send(item);
						}
						
					} else if (error.isSet(sockets[i])) {
						writeln("socket error");
						sockets[i].close();
					}
				}
			}
		}
	}
}
